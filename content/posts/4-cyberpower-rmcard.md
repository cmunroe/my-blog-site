+++
date = 2024-05-08
description = "Configuring up a CyberPower UPS RMCard the right way!"
tags = ["CyberPower", "UPS", "RMCard"]
title = "Configuring CyberPower RMCard"
categories = ["Tutorials"]
+++

This is a guide to how I securely configure a CyberPower RMCard. By default, many settings are left to be desired. 

In this guide I assume the actual `power-related` settings are based on good engineering principles i.e. Keep it `default` stupid. 


## 1. Firmware Upgrade

When setting up a new device the first thing we should do is update it.

As of the time of this writing, you can download version 1.4.1 from here: `https://www.cyberpowersystems.com/product/software/firmware/rmcard205305-firmware-v141/`

After downloading, `unzip` the file. 

Login to your new RMCard and head to `System > About`. 

Place the `cpsrm2scfw_141.bin` file in the `Firmware Upload` field.

Place the `cpsrm2scdata_141.bin` file in the `Data Upload` field.

Select `Submit`.

The card will reboot, and take around `5 minutes` to reload.

Head back to `System > About` and verify the `Firmware Version` matches is `1.4.1`.

## 2. Time

NTP is a must with RMCards otherwise you won't be able to know when something occured in the logs.

Head over to `System > General > Time` and set the following values: 

* Using NTP Server : `Enabled`.
* Primary NTP Server : `Your server of choice`.
* Secondary NTP Server : `Your server of choice`.
* Update Interval: `1`.

Select `Apply`.

Now head over to `System > General > Daylight Savings Time`.

I live in the `US` so let's enable `Traditional US DST time (Second Sunday in March to First Sunday in November)`.

## 3. Device Name

Isn't everything in the server room? Not always, let's tidy up the name and location of the device. These settings will also be used by SNMP.

Head over to `System > General > Identification`. Set the following values:

* Name : `CA-UPS-01` I use the naming schema: `Location`-`Device Type`-`Unique Number`.
* Location : `Upstairs `Closet` or whatever you think is most accurate.
* Contact : `An E-Mail Address` Using an email address allows you to use this in conjunction with SNMP Syscontact.

## 4. Disable Viewer Account

By default, RMCards ship with a `Viewer` account that can be used to access your system. Since this is likely not needed by most, let's disable it.

Head over to `System > Security > Local Account` and uncheck `Viewer: Allow Access`.

## 5. Session Control (Logout After)

Head over to `System > Security > Session Control` and set the timeout to `10` minutes. Otherwise, you will be logged out every 3 minutes. Which is quite a pain.

## 6. SNMP

### SNMP v1

First head over to `System > Network Service > SNMPv1` and uncheck `allow access`. Then click `apply`.

Next select the `private` community and set it to `forbidden`. By default, RMCards come with an unsafe default SNMP config that comes with `read/write`.


### SNMP v3

Now head over to `System > Network Service > SNMPv3` and check `allow access`. Then click `apply`. 

To set up an SNMPv3 community I suggest the following:

* User : `nms`
* Authentication Protocol : `SHA`
* Authentication Password : Your choice between 16 and 31 characters.
* Privacy Protocol : `AES`
* Privacy Password : Your choice between 16 and 31 characters.
* IP Address : `0.0.0.0`

Afterwards, select `Apply`.

## 7. Secure Protocols

### HTTPS

Head on down to `System > Network Service > Web Service`, select `Enable HTTPS` and then click `Apply`.


### SSH

Head down to `System > Network Service > Console Service`, select `Enable SSH` and then click `Apply`.


## 8. E-Mail Notifications

It's always nice to know the power is out, or that the UPS is having issues. 

### SMTP Configure

Head on over to `System > Notification > SMTP Server` and fill out based on your needs.

### Recipients

Head next to `System > Notification > E-mail Recipients` and add your email address. Make sure to `test` the setup before continuing.


## 9. Syslog

The final step! Let's jump to `Log > Syslog` and check `Syslog Enabled` then click `Apply`.

Next, click `Add` Server` and add the syslog server of your choice. 

## 10. Network Configuration

This is going to be much more dependent on your setup. In my case, I'd suggest a DHCP reservation so it is easier to update or migrate around later on.

With DHCP reservations, the defaults on the RMCard are fine so make your adjustments on your `dhcp server`.

In any case, place your RMcard on a `secured` network. Do no place this on your open workstation network if you can. 

## A Checklist

| Settings            | Location                                 | Default Value          | Suggested Value            | Notes                                                               | Completed |
|---------------------|------------------------------------------|------------------------|----------------------------|---------------------------------------------------------------------|-----------|
| NTP Server          | System > General > Time                  | Not Set                | Your NTP Servers           | Without it you can't determine easily what time something happened. |           |
| NTP Update Interval | System > General > Time                  | 8760                   | 1                          | 8760 Hours is once a year, which is silly.                          |           |
| Timezone            | System > General > Time                  | UTC                    | Set based on your location | Makes it easier to read logs.                                       |           |
| System Name         | System > General > Identification        | RMCARD205              |                            |                                                                     |           |
| System Location     | System > General > Identification        | Server Room            |                            |                                                                     |           |
| System Contact      | System > General > Identification        | Administrator          |                            |                                                                     |           |
| DST                 | System > General > Daylight Savings Time | Disabled               | Set based on your location | Automatic DST swap if needed.                                       |           |
| Login Timeout       | System > Security > Session Control      | 3                      | 10                         | Logout is too frequent when using the device.                       |           |
| Viewer Account      | System > Security > Local Account        | Enabled                | Disabled                   | Security: Default account on all CyberPower Devices.                |           |
| SNMP V1 Read/Write  | System > Network Service > SNMPv1        | Private Read/Write     | Private Forbidden          | Security: Read/Write SNMP default on all CyberPower.                |           |
| SNMP V1 Enabled     | System > Network Service > SNMPv1        | Allow Access: Enabled  | Allow Access: Disabled     | Security: SNMP V1 is in plain text.                                 |           |
| SNMP V3 Enable      | System > Network Service > SNMPv3        | Allow Access: Disabled | Allow Access: Enabled      | Security: SNMP v1 Alterntiave.                                      |           |
| HTTPS               | System > Network Service > Web           | Enable HTTP            | Enable HTTPS               | Security: Password sent in plain text without HTTPS.                |           |
| SSH                 | System > Network Service > Console       | Enable Telnet          | Enable SSH                 | Security: Telnet is in plain text.                                  |           |
| SMTP                | System > Notification > SMTP             | Disabled               | Enable                     | Notifications                                                       |           |
| Syslog              | Log > Syslog                             | Disabled               | Enable                     | Log Management                                                      |           |