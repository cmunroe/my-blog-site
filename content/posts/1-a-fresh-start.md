+++
date = 2024-04-10
description = "A Fresh Start with MiniPCs and more?"
tags = ["minipcs", "colocation"]
title = "A Fresh Start"
categories = ["servers"]
thumbnail = "/imgs/p/1-a-fresh-start/minipc.jpg"
+++

![MiniPC](/imgs/p/1-a-fresh-start/minipc.jpg)

Over a decade ago I colocated my first servers. It was a Dell PowerEdge 2950 RII, and it was a very good learning tool for an 18-year-old me. I think I was running Xen Server 5.5. After a few years, I moved from Xen Server to Proxmox. Who needs VMWare ESXi anyway? KVM with KSM made such an impact on me, allowing me to run more VMs and begin the all-to-common IT build-out of more and more services. At first, you just need an electric saw but then you sure want a drill press right afterward, right?

After my PowerEdge, I got myself a few E3-1230v2 servers with Proxmox. Though, technically they were my second batch. These servers are still chugging along nearly a decade after they were made. However, as these machines have aged so have their fans. 90c+ temps generally aren't the numbers you like to see on your server as it cranks away at the K3s cluster you decided to saddle it with. 

With the growing temperatures, and the price-to-performance ratio diminishing it might be time for another leap. Though I doubt I will fully be able to transition everything to a MiniPC cluster, so I will likely go with a hybrid approach. Sometimes you just need 256 GB of RAM in one server, right? 

With that... this blog, it's honestly a journal of this change and hopefully my projects afterward. Might sprinkle in some, now hilarious, learning experiences I have had along the way.