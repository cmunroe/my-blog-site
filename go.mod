module gitlab.com/pages/hugo

go 1.20

require (
	github.com/leafee98/hugo-theme-flat v0.0.0-20240123130918-36c140b28cc3 // indirect
	gitlab.com/writeonlyhugo/writeonlyhugo-theme v0.0.0-20230804022106-0464029c15e3 // indirect
)
